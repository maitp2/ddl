upperlim=10

for ((i=1; i<=upperlim; i++)); do
        echo "Sending jar file to VM $i"

        if [ "$i" -lt 10 ]; then
          scp out/artifacts/ddl_jar/sdfs.jar maitp2@fa22-cs425-520$i.cs.illinois.edu:/home/maitp2/
          ssh maitp2@fa22-cs425-520$i.cs.illinois.edu 'mkdir ./mp3_logs/'

        else
          scp out/artifacts/ddl_jar/sdfs.jar maitp2@fa22-cs425-52$i.cs.illinois.edu:/home/maitp2/
          ssh maitp2@fa22-cs425-52$i.cs.illinois.edu 'mkdir ./mp3_logs/'
        fi
done
